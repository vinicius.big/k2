# Issues

## Ubuntu OS

### Problem 1
- No keystroke or wrong keystrokes when pressing keys. This issue happened on Ubuntu 18.04, with no hwe kernel or hwe graphic/input drivers.


#### Solution

- Add a `keychron udev rules` in `/etc/udev/rules.d/` by running following commands:

```bash
echo 'SUBSYSTEMS=="input", ATTRS{name}=="Keychron K2", RUN+="echo 0 | tee /sys/module/hid_apple/parameters/fnmode"' | sudo tee /etc/udev/rules.d/80-keychron.rules
```
```bash
sudo udevadm control --reload-rules && sudo udevadm trigger
```


#### Source
- https://www.facebook.com/photo.php?fbid=2589685641075471&set=pcb.658885887922639
- https://gist.github.com/ercoppa/87a42a5d1fd65539844d7badc276d8e7?fbclid=IwAR3q7WhjIO2S6RAbfHWeq6xdZNBb75RestI9DJ9WBZAaPmQ_Fpyj3kSfDe8


### Problem 2
- F keys always output fn keys, even when pressing fn button. This happened on Ubuntu 19.04.

#### Solution

1. Edit or create the file /etc/modprobe.d/hid_apple.conf, e.g.:

```bash
sudo vim /etc/modprobe.d/hid_apple.conf
```

2. Add this line to the previously open file.

```
options hid_apple fnmode=2
```

3. Save the file and execute the following command to notify hid_apple module to reload it's configuration.

```bash
sudo update-initramfs -u
```

4. Reboot

#### Source

- https://www.facebook.com/groups/keychron/permalink/664894873988407/
- https://gist.github.com/mid9commander/669273
